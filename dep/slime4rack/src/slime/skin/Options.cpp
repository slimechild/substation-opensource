/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/skin/Options.hpp>

#include <window/Svg.hpp>

namespace slime {
namespace skin {
namespace detail {

std::string toString(const Size size) {
	switch (size) {
		case Size::SIZE_0_12_IN:
			return "0_12in";
		case Size::SIZE_0_2_IN:
			return "0_2in";
		case Size::SIZE_0_24_IN:
			return "0_24in";
		case Size::SIZE_0_4_IN:
			return "0_4in";
		case Size::SIZE_0_7_IN:
			return "0_7in";
		case Size::SIZE_0_8_IN:
			return "0_8in";
		default:
			assertEnum(Size, size);
	}
	return "";
}

rack::math::Vec toVec(const Size size) {
	float sizein = 0.0f;
	switch (size) {
		case Size::SIZE_0_12_IN:
			sizein = 0.12f;
			break;
		case Size::SIZE_0_2_IN:
			sizein = 0.2f;
			break;
		case Size::SIZE_0_24_IN:
			sizein = 0.24f;
			break;
		case Size::SIZE_0_4_IN:
			sizein = 0.4f;
			break;
		case Size::SIZE_0_7_IN:
			sizein = 0.7f;
			break;
		case Size::SIZE_0_8_IN:
			sizein = 0.8f;
			break;
		default:
			assertEnum(Size, size);
	}
	return rack::window::in2px(rack::math::Vec(sizein, sizein));
}

std::string toString(const Color color) {
	switch (color) {
		case Color::CLEAR:
			return "Clear";
		case Color::BLACK_PLASTIC:
			return "BlackPlastic";
		case Color::ORANGE:
			return "Orange";
		case Color::METAL:
			return "Metal";
		case Color::GREY:
			return "Grey";
		case Color::MINT:
			return "Mint";
		case Color::BLUE:
			return "Blue";
		case Color::GOLD:
			return "Gold";
		case Color::GREEN:
			return "Green";
		case Color::RED:
			return "Red";
		case Color::VIOLET:
			return "Violet";
		default:
			assertEnum(Color, color);
	}
	return "";
}

std::string toString(const Orientation orientation) {
	switch (orientation) {
		case Orientation::HORIZONTAL:
			return "Horizontal";
		case Orientation::VERTICAL:
			return "Vertical";
		default:
			assertEnum(Orientation, orientation);
	}
	return "";
}

}  // namespace detail
}  // namespace skin
}  // namespace slime