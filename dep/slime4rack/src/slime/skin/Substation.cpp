/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/skin/Substation.hpp>

#include <slime/Assert.hpp>
#include <slime/Color.hpp>

#include <string>
#include <utility>

#include <nanovg.h>
#include <asset.hpp>
#include <helpers.hpp>
#include <plugin/Plugin.hpp>

extern rack::plugin::Plugin* pluginInstance;

namespace slime {
namespace skin {
namespace substation {

/* Skin constants */
const std::string RESOURCE_SUFFIX = "4x.png";
const std::string COMPONENT_RESOURCE_DIR = "res/slime4rack/skin/substation/";
const std::vector<NVGcolor> CABLE_COLORS = {slime::hexToColor("6b7d72"), slime::hexToColor("8db580"),
											slime::hexToColor("c2cfb2"), slime::hexToColor("d8d1c9")};

/* Components */

// Lights
MonochromeLED::MonochromeLED(const Size size, const Color light_color) {
	auto box_size = detail::toVec(static_cast<detail::Size>(size));
	std::string prefix = COMPONENT_RESOURCE_DIR + "LED_" + detail::toString(static_cast<detail::Size>(size)) + "_" +
						 detail::toString(static_cast<detail::Color>(light_color)) + "_";

	setBackdropImage(rack::asset::plugin(pluginInstance, prefix + RESOURCE_SUFFIX), box_size);
	setHaloImage(rack::asset::plugin(pluginInstance, prefix + "Light_" + RESOURCE_SUFFIX), box_size * 3.0f);
}

// Button
Button::Button(const Size size, const OffColor off_color, const OnColor on_color) {
	auto box_size = detail::toVec(static_cast<detail::Size>(size));
	std::string prefix = COMPONENT_RESOURCE_DIR + "Button_" + detail::toString(static_cast<detail::Size>(size)) + "_";
	std::string off = detail::toString(static_cast<detail::Color>(off_color)) + "_";
	std::string on = detail::toString(static_cast<detail::Color>(on_color)) + "_";

	setFrames({rack::asset::plugin(pluginInstance, prefix + off + RESOURCE_SUFFIX),
			   rack::asset::plugin(pluginInstance, prefix + on + RESOURCE_SUFFIX)},
			  box_size);
}

void Button::onChange(const rack::event::Change& e) {
	// Sets the index
	app::ImageFrameSwitch::onChange(e);

	if (getIndex() == 0) {
		// Off
		_iw->useDrawMethod();
	} else {
		// On
		_iw->useDrawLayerMethod(1);  // 1 = glow
	}
}

// Icons
BrandLogo::BrandLogo(const Size size) : component::SvgIcon(getPath(size)) {}

void BrandLogo::attach(rack::app::ModuleWidget* widget) {
	setPositionPx(widget->box.size.x * 0.5f, rack::app::RACK_GRID_HEIGHT - rack::app::RACK_GRID_WIDTH * 1.05f);
	center();
	SvgIcon::attach(widget);
}

std::string BrandLogo::getPath(const Size size) {
	return rack::asset::plugin(
		pluginInstance, COMPONENT_RESOURCE_DIR + "Logo_" + detail::toString(static_cast<detail::Size>(size)) + ".svg");
}

}  // namespace substation
}  // namespace skin
}  // namespace slime
