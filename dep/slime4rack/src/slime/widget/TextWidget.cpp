/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/widget/TextWidget.hpp>

#include <context.hpp>

namespace slime {
namespace widget {

const std::string TextWidget::DEFAULT_FONT_PATH = "~|default|~";

TextWidget::TextWidget() {
	setDefaultFont();
	setAlignment(Horizontal::LEFT, Vertical::BASELINE);
	setColor(0.0f, 0.0f, 0.0f);
}

void TextWidget::setFont(const std::string& path) {
	if (_fontPath == path)
		return;

	_fontPath = path;
	_fontData.reset();
	wrap();
}

void TextWidget::setText(const std::string& text) {
	_contents = text;

	const char* ptr = _contents.c_str();
	std::size_t pos = 0;
	std::size_t index = 0;
	for (; index < MAX_ROWS; index++) {
		_rows[index].start = ptr + pos;
		pos = _contents.find('\n', pos);

		if (pos == std::string::npos) {
			_rows[index].end = nullptr;
			break;
		} else {
			_rows[index].end = ptr + pos;
			pos++;
		}
	}
	_rows[MAX_ROWS - 1].end = nullptr;
	_rowCount = index + 1;

	wrap();
}

void TextWidget::setAlignment(const Horizontal horiz, const Vertical vert) {
	_align = static_cast<int>(horiz) | static_cast<int>(vert);
}

void TextWidget::setSize(const float size) {
	_size = size;
	wrap();
}

void TextWidget::wrap() {
	_rewrap = true;
}

void TextWidget::draw(const DrawArgs& args) {
	if (_contents.size() == 0) {
		return;
	}

	if (!_fontData) {
		loadFontData();
	}

	nvgFontSize(args.vg, _size);
	nvgFontFaceId(args.vg, _fontData->handle);
	nvgFillColor(args.vg, _color);
	nvgTextAlign(args.vg, _align);

	float y = 0.0f;

	/*
		bool middle = (_align & static_cast<int>(Vertical::MIDDLE)) != 0;
		bool bottom = (_align & static_cast<int>(Vertical::BOTTOM)) != 0;
		if (middle || bottom) {
			float height = 0.0f;
			for (std::size_t index = 0; index < _rowCount; index++) {
				height += _lineSpacing * _size;
			}

			if (middle) {
				y -= height * 0.5f;
			} else if (bottom) {
				y -= height;
			}
		}
		*/

	for (std::size_t index = 0; index < _rowCount; index++) {
		nvgText(args.vg, 0.0f, y, _rows[index].start, _rows[index].end);
		y += _lineSpacing * _size;
	}
}

void TextWidget::onContextDestroy(const ContextDestroyEvent& e) {
	// Cannot have any references across context recreation, so release here
	_fontData.reset();
	Widget::onContextDestroy(e);
};

void TextWidget::loadFontData() {
	if (_fontPath == DEFAULT_FONT_PATH) {
		_fontData = APP->window->uiFont;
	} else {
		_fontData = APP->window->loadFont(_fontPath);
	}
}

}  // namespace widget
}  // namespace slime
