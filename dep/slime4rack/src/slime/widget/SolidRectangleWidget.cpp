/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include <slime/widget/SolidRectangleWidget.hpp>

namespace slime {
namespace widget {

void SolidRectangleWidget::draw(const DrawArgs& args) {
	nvgBeginPath(args.vg);
	nvgRect(args.vg, 0, 0, box.size.x, box.size.y);
	nvgFillColor(args.vg, _color);
	nvgFill(args.vg);
	nvgClosePath(args.vg);
}

}  // namespace widget
}  // namespace slime
