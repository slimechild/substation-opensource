/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.2.0
 */

#pragma once

#include <string>

#include <nanovg.h>

namespace slime {

// Convert a 6- or 8-character hex string (e.g. "ABC123") to an NVGColor.
// Should not include a leading #.
NVGcolor hexToColor(const std::string& color);

}  // namespace slime