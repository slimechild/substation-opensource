/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 1.0.0
 */

#pragma once

#include <cstddef>
#include <string>

#include <slime/widget/TextWidget.hpp>

#include <app/Knob.hpp>
#include <app/ModuleWidget.hpp>
#include <context.hpp>
#include <engine/Engine.hpp>
#include <engine/Port.hpp>
#include <math.hpp>
#include <widget/FramebufferWidget.hpp>
#include <widget/SvgWidget.hpp>

namespace slime {
namespace skin {
namespace component {

template <typename T, typename Base>
struct ComponentBase : protected Base {
	static_assert(std::is_base_of<rack::widget::Widget, Base>::value, "Base of ComponentBase not derived from Widget");

	T* setPositionPx(const rack::math::Vec& pos) {
		this->box.pos = pos;
		return static_cast<T*>(this);
	}

	inline T* setPositionPx(const float x, const float y) { return setPositionPx(rack::math::Vec(x, y)); }

	T* setPositionMm(const rack::math::Vec& pos) {
		this->box.pos = rack::window::mm2px(pos);
		return static_cast<T*>(this);
	}

	inline T* setPositionMm(const float x, const float y) { return setPositionMm(rack::math::Vec(x, y)); }

	T* setPositionIn(const rack::math::Vec& pos) {
		this->box.pos = rack::window::in2px(pos);
		return static_cast<T*>(this);
	}

	inline T* setPositionIn(const float x, const float y) { return setPositionIn(rack::math::Vec(x, y)); }

	T* center() {
		this->box.pos = this->box.pos.minus(this->box.size.mult(0.5f));
		return static_cast<T*>(this);
	}

	template <typename... Args>
	static T* makeComponent(Args... args) {
		return new T(args...);
	}

	// Attach to the module, transferring ownership
	void attach(rack::app::ModuleWidget* widget) { widget->addChild(static_cast<T*>(this)); }

private:
	using Base::setPosition;
};

template <typename T, typename Base>
struct AttachableBase : ComponentBase<T, Base> {
	// Attach to the module, transferring ownership
	virtual void attach(rack::engine::Module* module, std::size_t id, rack::app::ModuleWidget* widget) = 0;
};

template <typename T, typename Base>
struct ParamBase : AttachableBase<T, Base> {
	// Attach to the module, transferring ownership
	void attach(rack::engine::Module* module, std::size_t id, rack::app::ModuleWidget* widget) override {
		auto param = static_cast<T*>(this);
		param->rack::app::ParamWidget::module = module;
		param->rack::app::ParamWidget::paramId = id;
		param->initParamQuantity();
		widget->addParam(param);
	};
};

template <typename T, typename Base>
struct KnobComponent : ParamBase<T, Base> {
	T* setRange(float minAng, float maxAng) {
		auto knob = static_cast<T*>(this);
		knob->rack::app::Knob::minAngle = minAng;
		knob->rack::app::Knob::maxAngle = maxAng;
		return knob;
	}

	T* setSnapping(bool _snap) {
		auto knob = static_cast<T*>(this);
		knob->rack::app::Knob::snap = _snap;
		return knob;
	}
};

template <typename T, typename Base>
struct SwitchComponent : ParamBase<T, Base> {
	T* setMomentary(bool mom) {
		auto sw = static_cast<T*>(this);
		sw->rack::app::Switch::momentary = mom;
		return sw;
	}
};

template <typename T, typename Base>
struct PortComponent : AttachableBase<T, Base> {
	T* setInput() {
		auto port = static_cast<T*>(this);
		port->rack::app::PortWidget::type = rack::engine::Port::Type::INPUT;
		return port;
	}

	T* setOutput() {
		auto port = static_cast<T*>(this);
		port->rack::app::PortWidget::type = rack::engine::Port::Type::OUTPUT;
		return port;
	}

	// Attach to the module, transferring ownership
	void attach(rack::engine::Module* module, std::size_t id, rack::app::ModuleWidget* widget) override {
		T* port = static_cast<T*>(this);
		port->rack::app::PortWidget::module = module;
		port->rack::app::PortWidget::portId = static_cast<int>(id);
		if (port->rack::app::PortWidget::type == rack::engine::Port::Type::OUTPUT) {
			widget->addOutput(port);
		} else {
			widget->addInput(port);
		}
	}
};

template <typename T, typename Base>
struct MonochromeLightComponent : AttachableBase<T, Base> {
	// Attach to the module, transferring ownership
	void attach(rack::engine::Module* module, std::size_t id, rack::app::ModuleWidget* widget) override {
		T* light = static_cast<T*>(this);
		light->slime::app::ImageLight::module = module;
		light->slime::app::ImageLight::firstLightId = static_cast<int>(id);
		widget->addChild(light);
	}
};

// Icon
struct SvgIcon : component::ComponentBase<SvgIcon, rack::widget::SvgWidget> {
	SvgIcon(const std::string& path) { rack::widget::SvgWidget::setSvg(rack::window::Svg::load(path)); }

	static SvgIcon* makeComponent(const std::string& path) { return new SvgIcon(path); }
};

// Text
struct TextLabel : component::ComponentBase<TextLabel, slime::widget::TextWidget> {
	using slime::widget::TextWidget::Horizontal;
	using slime::widget::TextWidget::Vertical;

	TextLabel(const std::string& contents) { slime::widget::TextWidget::setText(contents); }

	TextLabel* setAlignment(const TextLabel::Horizontal& horizontal, const TextLabel::Vertical& vertical) {
		slime::widget::TextWidget::setAlignment(horizontal, vertical);
		return this;
	}

	TextLabel* setColor(float r, float g, float b, float a = 1.0f) {
		slime::widget::TextWidget::setColor(r, g, b, a);
		return this;
	}

	TextLabel* setFont(const std::string& path) {
		slime::widget::TextWidget::setFont(path);
		return this;
	}

	TextLabel* setDefaultFont() {
		slime::widget::TextWidget::setDefaultFont();
		return this;
	}

	TextLabel* setSize(const float size) {
		slime::widget::TextWidget::setSize(size);
		return this;
	}

	TextLabel* center() { return setAlignment(TextLabel::Horizontal::CENTER, TextLabel::Vertical::MIDDLE); }

	static TextLabel* makeComponent(const std::string& contents) { return new TextLabel(contents); }
};

//

}  // namespace component
}  // namespace skin
}  // namespace slime
