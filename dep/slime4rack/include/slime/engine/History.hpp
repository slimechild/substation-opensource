/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.2
 */

#pragma once

#include <string>

#include <slime/IntellisenseFix.hpp>

#include <app/ModuleWidget.hpp>
#include <engine/Module.hpp>
#include <history.hpp>

namespace slime {
namespace engine {

class ModuleHistoryGuard {
public:
	ModuleHistoryGuard(rack::engine::Module* module);
	ModuleHistoryGuard(rack::engine::Module* module, const std::string& name);
	~ModuleHistoryGuard();

private:
	rack::engine::Module* _module;
	rack::history::ModuleChange* _history;
};

}  // namespace engine
}  // namespace slime
