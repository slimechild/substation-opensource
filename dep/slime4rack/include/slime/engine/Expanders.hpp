/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: v2.0.4
 */

#pragma once

#include <cstddef>
#include <string>

#include <engine/Module.hpp>
#include <plugin/Model.hpp>

namespace slime {
namespace engine {

typedef std::nullptr_t EmptyMessage;

template <typename LEFT_OUT, typename LEFT_IN, typename RIGHT_OUT = LEFT_OUT, typename RIGHT_IN = LEFT_IN>
class ExpandableModule : public Module {
public:
	using right_outgoing_type = RIGHT_OUT;
	using right_incoming_type = RIGHT_IN;
	using left_outgoing_type = LEFT_OUT;
	using left_incoming_type = LEFT_IN;

	ExpandableModule() {
		Module::leftExpander.producerMessage = &_left_data[0];
		Module::leftExpander.consumerMessage = &_left_data[1];

		Module::rightExpander.producerMessage = &_right_data[0];
		Module::rightExpander.consumerMessage = &_right_data[1];

		_left_expander_cache = nullptr;
		_right_expander_cache = nullptr;
		_has_left = false;
		_has_right = false;
	}

	virtual bool matchLeftSlug(const std::string& value);

	virtual bool matchRightSlug(const std::string& value);

	inline bool hasLeftExpander() { return _has_left; }

	inline bool hasRightExpander() { return _has_right; }

	// Warning: Do not cache this value! Call on every access!
	inline left_outgoing_type* toLeftExpander() {
		return reinterpret_cast<left_outgoing_type*>(Module::leftExpander.producerMessage);
	}

	// Warning: Do not cache this value! Call on every access!
	inline right_outgoing_type* toRightExpander() {
		return reinterpret_cast<right_outgoing_type*>(Module::rightExpander.producerMessage);
	}

	// Warning: Do not cache this value! Call on every access!
	inline left_incoming_type* fromLeftExpander() {
		return reinterpret_cast<left_incoming_type*>(_left_expander_cache->rightExpander.consumerMessage);
	}

	// Warning: Do not cache this value! Call on every access!
	inline right_incoming_type* fromRightExpander() {
		return reinterpret_cast<right_incoming_type*>(_right_expander_cache->leftExpander.consumerMessage);
	}

	bool updateExpanders() {
		bool changed = false;

		// Check left
		if (_left_expander_cache != Module::leftExpander.module) {
			_left_expander_cache = Module::leftExpander.module;
			if ((_left_expander_cache != nullptr) && matchLeftSlug(_left_expander_cache->model->slug) &&
				(_left_expander_cache->rightExpander.consumerMessage != nullptr)) {
				changed = true;
				_has_left = true;
			} else if ((_left_expander_cache == nullptr) || !matchLeftSlug(_left_expander_cache->model->slug) ||
					   (_left_expander_cache->rightExpander.consumerMessage == nullptr)) {
				changed = true;
				_has_left = false;
			}
		}

		// Check right
		if (_right_expander_cache != Module::rightExpander.module) {
			_right_expander_cache = Module::rightExpander.module;
			if ((_right_expander_cache != nullptr) && matchRightSlug(_right_expander_cache->model->slug) &&
				(_right_expander_cache->leftExpander.consumerMessage != nullptr)) {
				changed = true;
				_has_right = true;
			} else if ((_right_expander_cache == nullptr) || !matchRightSlug(_right_expander_cache->model->slug) ||
					   (_right_expander_cache->leftExpander.consumerMessage == nullptr)) {
				changed = true;
				_has_right = false;
			}
		}

		return changed;
	}

	void sendExpanderMessages() {
		// Swap left
		if (_has_left) {
			Module::leftExpander.messageFlipRequested = true;
		}

		// Swap right
		if (_has_right) {
			Module::rightExpander.messageFlipRequested = true;
		}
	}

private:
	left_outgoing_type _left_data[2]{left_outgoing_type(), left_outgoing_type()};
	right_outgoing_type _right_data[2]{right_outgoing_type(), right_outgoing_type()};
	rack::engine::Module* _left_expander_cache;
	rack::engine::Module* _right_expander_cache;
	bool _has_left, _has_right;
};

}  // namespace engine
}  // namespace slime
