/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: v1.1.0
 */

#pragma once

#include <cstddef>
#include <string>

#include <slime/Common.hpp>

#include <GLFW/glfw3.h>
#include <jansson.h>
#include <context.hpp>
#include <logger.hpp>
#include <window/Window.hpp>

namespace slime {
namespace engine {

class Clipboard {
public:
	Clipboard() = delete;

	template <typename T>
	static T get();

	template <typename T>
	static void set(T value);

	// Special case for conversions with flags
	template <typename T>
	static void set(T value, std::size_t flags);
};

// Return a bare pointer. Do not free this pointer; GLFW will handle it automatically.
template <>
const char* Clipboard::get<const char*>() {
	const char* res = glfwGetClipboardString(APP->window->win);

#if defined(SLIME_DEBUG)
	DEBUG("Read from clipboard: %s", res);
#endif

	return res;
}

// Cast to a string
template <>
std::string Clipboard::get<std::string>() {
	auto ptr = Clipboard::get<const char*>();
	if (ptr) {
		return std::string(ptr);
	} else {
		WARN("Could not get clipboard contents.");
		return std::string();
	}
}

// Interpret as a json object, if possible, or null otherwise
template <>
json_t* Clipboard::get<json_t*>() {
	auto ptr = Clipboard::get<const char*>();
	if (!ptr) {
		WARN("Could not get clipboard contents.");
		return nullptr;
	}

	json_error_t error;
	json_t* doc = json_loads(ptr, 0, &error);
	if (!doc) {
		WARN("Clipboard does not contain valid JSON data: %s", error.text);
		return nullptr;
	}

	return doc;
}

// Set a bare pointer.
template <>
void Clipboard::set<const char*>(const char* value) {
#if defined(SLIME_DEBUG)
	DEBUG("Writing to clipboard: %s", value);
#endif

	glfwSetClipboardString(APP->window->win, value);
}

// Set to a string
template <>
void Clipboard::set<std::string>(std::string value) {
	Clipboard::set<const char*>(value.c_str());
}

// Set from json, if possible
template <>
void Clipboard::set<json_t*>(json_t* value, std::size_t flags) {
	char* ptr = json_dumps(value, flags);
	if (!ptr) {
		WARN("Could not dump to JSON.");
		return;
	}

	Clipboard::set<const char*>(ptr);

	free(ptr);
}

// Set from json, if possible, with default flags
template <>
void Clipboard::set<json_t*>(json_t* value) {
	std::size_t flags = JSON_COMPACT | JSON_REAL_PRECISION(9);
	Clipboard::set<json_t*>(value, flags);
}

}  // namespace engine
}  // namespace slime
