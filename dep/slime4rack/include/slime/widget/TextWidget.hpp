/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: unstable
 * Compatibility: 2.0.0
 *
 * FUTURE: void nvgTextLetterSpacing(NVGcontext* ctx, float spacing);
 * FUTURE: void nvgTextLineHeight(NVGcontext* ctx, float lineHeight);
 * FUTURE: break width
 * FUTURE: nvgTextBreakLines
 * TODO: can't resize box without a context. this might be ok, though.
 */

#pragma once

#include <cstddef>
#include <memory>
#include <string>

#include <nanovg.h>
#include <app/common.hpp>
#include <context.hpp>
#include <widget/Widget.hpp>
#include <window/Window.hpp>

namespace slime {
namespace widget {

class TextWidget : public rack::widget::Widget {
public:
	enum class Horizontal { LEFT = NVG_ALIGN_LEFT, CENTER = NVG_ALIGN_CENTER, RIGHT = NVG_ALIGN_RIGHT };
	enum class Vertical {
		TOP = NVG_ALIGN_TOP,
		MIDDLE = NVG_ALIGN_MIDDLE,
		BOTTOM = NVG_ALIGN_BOTTOM,
		BASELINE = NVG_ALIGN_BASELINE
	};

	TextWidget();

	void wrap();

	void setText(const std::string& text);
	void setAlignment(const Horizontal horiz, const Vertical vert);
	inline void setColor(const NVGcolor& color) { _color = color; };
	inline void setColor(float r, float g, float b, float a = 1.0f) { _color = nvgRGBAf(r, g, b, a); };
	void setSize(const float size);
	void setFont(const std::string& path);
	inline void setDefaultFont() { setFont(DEFAULT_FONT_PATH); };

	void draw(const DrawArgs& args) override;
	void onContextDestroy(const ContextDestroyEvent& e) override;
	// void onContextCreate(const ContextCreateEvent& e) override;  // This doesn't seem to work

protected:
	struct RowData {
		const char* start;
		const char* end;
	};

	static const std::size_t MAX_ROWS = 128;
	static const std::string DEFAULT_FONT_PATH;

	std::string _contents;
	std::string _fontPath;
	std::shared_ptr<rack::window::Font> _fontData = nullptr;
	float _size = 13.0f;
	NVGcolor _color;
	int _align;
	bool _rewrap = false;
	float _lineSpacing = 1.0f;
	RowData _rows[MAX_ROWS];
	size_t _rowCount;

	void loadFontData();
};

}  // namespace widget
}  // namespace slime
