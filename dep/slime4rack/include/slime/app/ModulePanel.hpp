/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <string>

#include <slime/widget/BorderWidget.hpp>
#include <slime/widget/ImageWidget.hpp>
#include <slime/widget/SolidRectangleWidget.hpp>

#include <nanovg.h>
#include <app/ModuleWidget.hpp>
#include <widget/FramebufferWidget.hpp>

namespace slime {
namespace app {

// Implements a module panel that can combine an image background, an SVG background, and a border.
// Rounds its size to the nearest allowed.
class ModulePanel : public rack::widget::FramebufferWidget {
public:
	ModulePanel(std::size_t width);

	static ModulePanel* makePanel(std::size_t width) { return new ModulePanel(width); }
	ModulePanel* attach(rack::app::ModuleWidget* widget);

	inline std::size_t getWidthHP() const { return _width; }

	ModulePanel* addSolidBackground(const NVGcolor& color);
	inline ModulePanel* addSolidBackground(float r = 0.5f, float g = 0.5f, float b = 0.5f) {
		return addSolidBackground(nvgRGBAf(r, g, b, 1.0f));
	};

	ModulePanel* addImageBackground(const std::string& path);

	ModulePanel* addSvgBackground(const std::string& path);

	ModulePanel* addBorder(const NVGcolor& color);
	ModulePanel* addBorder(float r = 0.5f, float g = 0.5f, float b = 0.5f, float a = 0.5f);

	void step() override;

private:
	std::size_t _width;
};

}  // namespace app
}  // namespace slime
