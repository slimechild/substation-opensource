/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <memory>

#include <app/Knob.hpp>
#include <app/common.hpp>
#include <widget/FramebufferWidget.hpp>
#include <widget/TransformWidget.hpp>
#include <widget/Widget.hpp>

#include "../widget/ImageWidget.hpp"

namespace slime {
namespace app {

// Implements a knob that displays a fixed background image and rotating indicator.
class SimpleImageKnob : public rack::app::Knob {
public:
	float minAngle = -0.8f * M_PI;
	float maxAngle = +0.8f * M_PI;

	SimpleImageKnob();

	void setBackdropImage(const std::string& path, const rack::math::Vec& size);
	void setIndicatorImage(const std::string& path, const rack::math::Vec& size);

	void onChange(const rack::widget::Widget::ChangeEvent& e) override;

private:
	rack::widget::FramebufferWidget* _fb = nullptr;
	rack::widget::TransformWidget* _indicatorTransform = nullptr;
	widget::ImageWidget* _indicatorWidget = nullptr;
	widget::ImageWidget* _backgroundWidget = nullptr;
};

}  // namespace app
}  // namespace slime
