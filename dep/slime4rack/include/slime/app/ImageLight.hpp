/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 *
 * FUTURE: a better blending mode
 */

#pragma once

#include <memory>

#include <slime/IntellisenseFix.hpp>

#include <engine/Module.hpp>
#include <widget/Widget.hpp>

#include "../widget/ImageWidget.hpp"

namespace slime {
namespace app {

// Implements a light with a fixed background image and variable-brightness halo image
class ImageLight : public rack::widget::Widget {
public:
	rack::engine::Module* module = NULL;
	int firstLightId = 0;

	ImageLight();

	void setBackdropImage(const std::string& path, const rack::math::Vec& size);
	void setHaloImage(const std::string& path, const rack::math::Vec& size);

	// void draw(const DrawArgs& args) override;
	void drawLayer(const DrawArgs& args, int layer) override;

private:
	static const int _GLOW_LAYER = 1;

	widget::ImageWidget* _backgroundWidget = nullptr;
	widget::ImageWidget* _glowWidget = nullptr;
};

}  // namespace app
}  // namespace slime
