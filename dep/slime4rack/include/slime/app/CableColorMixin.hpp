/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of slime4rack and is governed by the attached license.
 *
 * Author: C. V. Pines
 * Status: stable
 * Compatibility: 2.0.0
 */

#pragma once

#include <cstddef>
#include <memory>
#include <type_traits>
#include <vector>

#include <nanovg.h>
#include <app/CableWidget.hpp>
#include <app/Scene.hpp>
#include <context.hpp>
#include <engine/Port.hpp>
#include <random.hpp>
#include <widget/Widget.hpp>

#include "ImagePort.hpp"

namespace slime {
namespace app {

// Provides an input/output port that creates cables with custom colors
// Provide ImagePort, SvgPort, etc as a template argument
template <typename Base>
class CableColorMixin : public Base {
	static_assert(std::is_base_of<rack::app::PortWidget, Base>::value,
				  "Base of CableColorMixin not derived from PortWidget");

public:
	CableColorMixin(void) : _index(rack::random::u32()) {}

	void setCustomColors(const std::shared_ptr<const std::vector<NVGcolor>>& colors) {
		if (colors == nullptr) {
			_colors_weak.reset();
			_initialized = false;
		} else {
			_colors_weak = colors;
			_initialized = true;
		}
	}

	void onDragStart(const rack::widget::Widget::DragStartEvent& e) override {
		Base::onDragStart(e);

		if (!_initialized) {
			return;
		}

		if (std::shared_ptr<const std::vector<NVGcolor>> colors_locked = _colors_weak.lock()) {
			if (!colors_locked->size())
				return;

			rack::app::CableWidget* cw = rack::contextGet()->scene->rack->getIncompleteCable();

			if (cw && (this->type == rack::engine::Port::Type::INPUT) == static_cast<bool>(cw->inputPort)) {
				_index = _index % colors_locked->size();
				cw->color = colors_locked->at(_index);
				_index++;
			}
		} else {
			WARN("Lifetime of CableColorMixin's color vector expired early");
		}
	}

private:
	bool _initialized = false;
	std::weak_ptr<const std::vector<NVGcolor>> _colors_weak;
	std::size_t _index = 0;
};

}  // namespace app
}  // namespace slime
