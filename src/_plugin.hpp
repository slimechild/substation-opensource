/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#pragma once

#include <slime/IntellisenseFix.hpp>
#include <slime/engine/Module.hpp>

#include <cstddef>
using std::size_t;

#include <app/ModuleWidget.hpp>
#include <app/common.hpp>
#include <asset.hpp>
#include <helpers.hpp>
#include <logger.hpp>
#include <math.hpp>
#include <plugin/Model.hpp>
#include <plugin/Plugin.hpp>
#include <plugin/callbacks.hpp>

#include <nanovg.h>
#include <vector>

extern rack::plugin::Plugin* pluginInstance;

namespace slime {
namespace plugin {
namespace substation {

extern rack::plugin::Model* modelClock;
extern rack::plugin::Model* modelPolySequencer;
extern rack::plugin::Model* modelSubOscillator;
extern rack::plugin::Model* modelMixer;
extern rack::plugin::Model* modelFilter;
extern rack::plugin::Model* modelEnvelopes;
extern rack::plugin::Model* modelQuantizer;
extern rack::plugin::Model* modelVCA;
extern rack::plugin::Model* modelBlank4;
extern rack::plugin::Model* modelBlank7;
extern rack::plugin::Model* modelFilterPlus;

}  // namespace substation
}  // namespace plugin
}  // namespace slime
