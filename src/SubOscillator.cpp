/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <cmath>
#include <memory>

#include <slime/Math.hpp>
#include <slime/app/ModulePanel.hpp>
#include <slime/dsp/MinBLEPSquare.hpp>
#include <slime/dsp/PhaseDistortionSaw.hpp>
#include <slime/skin/Ersatz.hpp>

#include <common.hpp>
#include <dsp/approx.hpp>
#include <dsp/common.hpp>
#include <dsp/digital.hpp>
#include <simd/Vector.hpp>
#include <simd/functions.hpp>
#include <slime/ui/UI.hpp>

typedef rack::simd::float_4 float_simd;

namespace slime {
namespace plugin {
namespace substation {

struct SubOscillatorModule : slime::engine::Module {
	enum Waveform { SQUARE, SAW_MOD_SQUARE, SAW, NUM_WAVEFORMS };

	// Immutable config
	const float DETUNE_CENTS_DISPLAY_MULTIPLIER = 10.0f;

	// Mutable config
	std::shared_ptr<bool> detune_cents;  // JSON serialized

	// Internal state
	slime::dsp::MinBLEPSquareOscillator<float_simd> sqr_osc;
	slime::dsp::PhaseDistortionSawOscillator<float_simd> saw_osc;
	rack::dsp::ClockDivider param_divider;
	float detune_hz, detune_mult, detune_mult_inv;
	float mod_phase;
	Waveform waveform;
	size_t sub_a_n, sub_b_n;
	float_simd output;

	enum ParamIds {
		BASE_FREQ_PARAM,
		WAVEFORM_PARAM,
		SUB_A_DIV_PARAM,
		SUB_B_DIV_PARAM,
		PWM_PARAM,
		DETUNE_PARAM,
		NUM_PARAMS
	};

	enum InputIds { BASE_FREQ_INPUT, SUB_A_DIV_INPUT, SUB_B_DIV_INPUT, PWM_INPUT, NUM_INPUTS };

	enum OutputIds { BASE_OUTPUT, SUB_A_OUTPUT, SUB_B_OUTPUT, NUM_OUTPUTS };

	enum LightIds { NUM_LIGHTS };

	SubOscillatorModule(void) {
		detune_cents = std::make_shared<bool>(false);

		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(BASE_FREQ_INPUT, "Base frequency");
		configInput(SUB_A_DIV_INPUT, "Sub 1 divider");
		configInput(SUB_B_DIV_INPUT, "Sub 2 divider");
		configInput(PWM_INPUT, "PWM");
		configOutput(BASE_OUTPUT, "Base");
		configOutput(SUB_A_OUTPUT, "Sub 1");
		configOutput(SUB_B_OUTPUT, "Sub 2");

		configParam(BASE_FREQ_PARAM, -48.0f, 48.0f, 0.f,  // +/- 4 octaves
					"Base Frequency", " Hz", rack::dsp::FREQ_SEMITONE, rack::dsp::FREQ_C4);
		configParam(SUB_A_DIV_PARAM, 1.0f, 16.0f, 1.0f, "Subdivision 1");
		configParam(SUB_B_DIV_PARAM, 1.0f, 16.0f, 1.0f, "Subdivision 2");
		configSwitch(WAVEFORM_PARAM, 0.0f, 2.0f, 2.0f, "Waveform", {"SQUARE", "SUB 1 MOD", "SAW"});
		configParam(PWM_PARAM, 0.0f, 1.0f, 0.5f, "PWM", "%", 0.0f, 100.0f);
		configParam(DETUNE_PARAM, -2.0f, 2.0f, 0.0f, "Detune", " Hz");

		updateDetuneParam();

		param_divider.setDivision(64);

		onReset();
	}

	void onReset() override {
		waveform = SAW;
		sub_a_n = static_cast<size_t>(-1);
		sub_b_n = static_cast<size_t>(-1);
		detune_hz = 0.0f;
		detune_mult = 1.0f;
		detune_mult_inv = 1.0f;
		param_divider.reset();
	}

	void process(const ProcessArgs& args) override {
		if (param_divider.process()) {
			// Set waveform type; sync
			Waveform new_waveform = static_cast<Waveform>(params[WAVEFORM_PARAM].getValue());
			if (new_waveform != waveform) {
				waveform = new_waveform;
				mod_phase = 0.0f;
				saw_osc.sync();
				sqr_osc.sync();
			}

			// Detune
			if (*detune_cents) {
				// 1200 cents per volt; knob uses *5 multiplier
				detune_mult =
					rack::dsp::approxExp2_taylor5(
						(params[DETUNE_PARAM].getValue() * (DETUNE_CENTS_DISPLAY_MULTIPLIER / 1200.0f)) + 1.0f) *
					0.5f;
				detune_mult_inv = 1.0f / detune_mult;
				detune_hz = 0.0f;
			} else {
				detune_mult = 1.0f;
				detune_mult_inv = 1.0f;
				detune_hz = params[DETUNE_PARAM].getValue();
			}

			// Divisions
			sub_a_n = static_cast<size_t>(rack::math::clamp(
				std::round(params[SUB_A_DIV_PARAM].getValue() + inputs[SUB_A_DIV_INPUT].getVoltage() * (8.0f / 5.0f)),
				1.0f, 16.0f));
			sub_b_n = static_cast<size_t>(rack::math::clamp(
				std::round(params[SUB_B_DIV_PARAM].getValue() + inputs[SUB_B_DIV_INPUT].getVoltage() * (8.0f / 5.0f)),
				1.0f, 16.0f));
		}

		// Pitch - full-rate because this can be FM'd
		float base_pitch = inputs[BASE_FREQ_INPUT].getVoltage() + params[BASE_FREQ_PARAM].getValue() / 12.0f;
		float base_freq = rack::dsp::FREQ_C4 * rack::dsp::approxExp2_taylor5(base_pitch + 14.0f) / 16384.0f;
		float_simd freq = float_simd(base_freq, (detune_mult * base_freq / sub_a_n) + detune_hz,
									 (detune_mult_inv * base_freq / sub_b_n) - detune_hz, 0.0f);

		// Saw and Mod both need the saw oscillator
		if (waveform == Waveform::SAW || waveform == Waveform::SAW_MOD_SQUARE) {
			// Coeffs chosen to minimize aliasing up to ~5kHz
			saw_osc.setDistortion(
				rack::simd::fmax(0.5f, static_cast<float_simd>(0.9924f) - static_cast<float_simd>(0.00008f) * freq));
			saw_osc.setFrequency(freq);

			saw_osc.process(args.sampleTime);

			if (waveform == Waveform::SAW) {
				output[0] = -saw_osc.value[0];
			}

			output[1] = -saw_osc.value[1];
			output[2] = -saw_osc.value[2];
		}

		// Square and Mod both need the square oscillator
		if (waveform == Waveform::SQUARE || waveform == Waveform::SAW_MOD_SQUARE) {
			float width = inputs[PWM_INPUT].getVoltage() * 0.1f + params[PWM_PARAM].getValue();

			// Keep track of a subdivided modulation phase using SUB1's division
			mod_phase += freq[1] * args.sampleTime;
			mod_phase -= std::floor(mod_phase);

			if (waveform == Waveform::SAW_MOD_SQUARE) {
				sqr_osc.channels = 1;

				if (sub_a_n > 1) {
					width += (mod_phase - 0.5f);
				}
			} else {
				sqr_osc.channels = 3;
			}

			sqr_osc.setFrequency(freq);
			sqr_osc.setPulseWidth(width);
			sqr_osc.process(args.sampleTime);

			output[0] = sqr_osc.value[0];

			if (waveform == Waveform::SQUARE) {
				output[1] = sqr_osc.value[1];
				output[2] = sqr_osc.value[2];
			}
		}

		outputs[BASE_OUTPUT].setVoltage(output[0] * 5.0f);
		outputs[SUB_A_OUTPUT].setVoltage(output[1] * 5.0f);
		outputs[SUB_B_OUTPUT].setVoltage(output[2] * 5.0f);
	}

	void updateDetuneParam() {
		auto* pq = getParamQuantity(DETUNE_PARAM);

		if (*detune_cents) {
			pq->unit = " cents";
			pq->displayMultiplier = DETUNE_CENTS_DISPLAY_MULTIPLIER;
		} else {
			pq->unit = " Hz";
			pq->displayMultiplier = 1.0f;
		}
	}

	json_t* dataToJson() override {
		json_t* root = json_object();

		// Detune in cents
		json_object_set_new(root, "detune_cents", json_boolean(*detune_cents));

		return root;
	}

	void dataFromJson(json_t* root) override {
		// Detune in cents
		json_t* detune_cents_json = json_object_get(root, "detune_cents");
		if (detune_cents_json) {
			*detune_cents = json_boolean_value(detune_cents_json);
			updateDetuneParam();
		}
	}
};

struct SubOscillatorWidget : rack::app::ModuleWidget {
	SubOscillatorWidget(SubOscillatorModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(13ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/SubOscillator_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/SubOscillator_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::FULL)->attach(this);

		// Inputs
		rack::math::Vec inputs[] = {{0.4f, 3.55f}, {1.0f, 3.55f}, {1.6f, 3.55f}, {2.2f, 3.55f}};
		for (size_t i = 0; i < SubOscillatorModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		rack::math::Vec outputs[] = {{0.4f, 4.2f}, {1.0f, 4.2f}, {1.6f, 4.2f}};
		for (size_t i = 0; i < SubOscillatorModule::NUM_OUTPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(outputs[i])
				->center()
				->setOutput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Knobs
		Knob::makeComponent(Knob::Size::BIG)
			->setPositionIn(0.85f, 1.1f)
			->center()
			->attach(module, SubOscillatorModule::BASE_FREQ_PARAM, this);

		SlideSwitch::makeComponent(SlideSwitch::Orientation::VERTICAL, SlideSwitch::Positions::TRIPLE_THROW)
			->setPositionIn(1.7f, 1.1f)
			->center()
			->attach(module, SubOscillatorModule::WAVEFORM_PARAM, this);

		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(0.4f, 2.15f)
			->center()
			->attach(module, SubOscillatorModule::DETUNE_PARAM, this);

		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(1.0f, 2.15f)
			->center()
			->setSnapping(true)
			->attach(module, SubOscillatorModule::SUB_A_DIV_PARAM, this);

		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(1.6f, 2.15f)
			->center()
			->setSnapping(true)
			->attach(module, SubOscillatorModule::SUB_B_DIV_PARAM, this);

		Knob::makeComponent(Knob::Size::MEDIUM)
			->setPositionIn(2.2f, 2.15f)
			->center()
			->attach(module, SubOscillatorModule::PWM_PARAM, this);
	}

	void appendContextMenu(rack::ui::Menu* menu) override {
		auto* pmodule = dynamic_cast<SubOscillatorModule*>(this->module);
		if (!pmodule) {
			FATAL("Module was not set when appending context menu; cannot bind callbacks.");
			return;
		}

		menu->addChild(new rack::ui::MenuSeparator);

		rack::ui::MenuLabel* modelLabel = new rack::ui::MenuLabel;
		modelLabel->text = "Module Settings";
		menu->addChild(modelLabel);

		{  // Detune in cents
			auto detune = new slime::ui::BoolSettingMenuItem<>(pmodule->detune_cents);
			detune->text = "Detune in cents";
			detune->getInvoker() += std::bind(&SubOscillatorModule::updateDetuneParam, pmodule);
			menu->addChild(detune);
		}

		settings.appendContextMenu(menu);
	}
};

rack::plugin::Model* modelSubOscillator =
	rack::createModel<SubOscillatorModule, SubOscillatorWidget>("SlimeChild-Substation-OpenSource-SubOscillator");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
