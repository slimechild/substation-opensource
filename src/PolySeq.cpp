/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <functional>

#include <slime/Common.hpp>
#include <slime/app/ModulePanel.hpp>
#include <slime/cv/Digital.hpp>
#include <slime/engine/Clipboard.hpp>
#include <slime/engine/History.hpp>
#include <slime/engine/ParamChangeHelper.hpp>
#include <slime/skin/Ersatz.hpp>
#include <slime/ui/UI.hpp>
#include <ui/common.hpp>

#include <jansson.h>  // for json
#include <string.h>   // for strncmp
#include <common.hpp>
#include <dsp/digital.hpp>
#include <math.hpp>
#include <random.hpp>

namespace slime {
namespace plugin {
namespace substation {

struct PolySequencerModule : slime::engine::Module {
	// Immutable config
	static const size_t MAX_SEQUENCE_STEPS = 4;  // Eventually boost this to 8
	static const size_t NUM_DIVIDERS = 4;
	static constexpr float MAX_RHYTHM_DIVISIONS = 16.0f;

	static const std::array<float, 3> SEQ_RANGE_SCALES;
	static constexpr float SQUELCH_TIME = 2.0e-4f;

	struct SavedSequencerState {
		size_t seq_a_state, seq_b_state, seq_c_state;
		std::array<size_t, NUM_DIVIDERS> rhythm_divider_states;

		SavedSequencerState() : seq_a_state(0), seq_b_state(0), seq_c_state(0) { rhythm_divider_states.fill(0); }

		void roll(size_t steps) {
			seq_a_state = (seq_a_state + 1) % steps;
			seq_b_state = (seq_b_state + 1) % steps;
			seq_c_state = (seq_c_state + 1) % steps;
		}
	};

	// Mutable config
	const size_t sequence_steps = MAX_SEQUENCE_STEPS;  // Fixed, for now
	std::shared_ptr<bool> reset_to_saved_state;        // JSON serialized

	// Internal state
	size_t seq_a_state = 0, seq_b_state = 0, seq_c_state = 0;          // JSON serialized
	std::array<rack::dsp::ClockDivider, NUM_DIVIDERS> rhythm_divider;  // JSON serialized
	std::array<bool, NUM_DIVIDERS> seq_a_divisions;
	std::array<bool, NUM_DIVIDERS> seq_b_divisions;
	std::array<bool, NUM_DIVIDERS> seq_c_divisions;
	std::array<bool, NUM_DIVIDERS> rhythm_divider_triggered;
	slime::cv::SchmittTrigger clock_trigger, reset_trigger;
	rack::dsp::PulseGenerator reset_squelch_pulse;
	rack::dsp::PulseGenerator seq_a_clock_pulse, seq_b_clock_pulse, seq_c_clock_pulse;
	rack::dsp::ClockDivider light_divider;
	slime::engine::ParamChangeHelper param_helper;
	SavedSequencerState saved_state, reset_hold_state;
	bool summing_xor = true;

	enum ParamIds {
		ENUMS(SEQ_A_PARAM, MAX_SEQUENCE_STEPS),
		ENUMS(SEQ_B_PARAM, MAX_SEQUENCE_STEPS),
		ENUMS(SEQ_C_PARAM, MAX_SEQUENCE_STEPS),
		ENUMS(DIVIDER_PARAM, NUM_DIVIDERS),
		ENUMS(DIVIDER_TO_SEQ_A_PARAM, NUM_DIVIDERS),
		ENUMS(DIVIDER_TO_SEQ_B_PARAM, NUM_DIVIDERS),
		ENUMS(DIVIDER_TO_SEQ_C_PARAM, NUM_DIVIDERS),
		SEQ_A_RANGE_PARAM,
		SEQ_B_RANGE_PARAM,
		SEQ_C_RANGE_PARAM,
		SUMMING_MODE_PARAM,
		RESET_PARAM,
		NEXT_PARAM,
		NUM_STEPS_PARAM,
		NUM_PARAMS
	};

	enum InputIds { CLK_INPUT, RESET_INPUT, ENUMS(DIVIDER_INPUT, NUM_DIVIDERS), NUM_INPUTS };

	enum OutputIds {
		SEQ_A_CLK_OUTPUT,
		SEQ_B_CLK_OUTPUT,
		SEQ_C_CLK_OUTPUT,
		SEQ_A_OUTPUT,
		SEQ_B_OUTPUT,
		SEQ_C_OUTPUT,
		NUM_OUTPUTS
	};

	enum LightIds {
		ENUMS(DIVIDER_TRIGGER_LIGHT, NUM_DIVIDERS),
		ENUMS(SEQ_A_STATE_LIGHT, MAX_SEQUENCE_STEPS),
		ENUMS(SEQ_B_STATE_LIGHT, MAX_SEQUENCE_STEPS),
		ENUMS(SEQ_C_STATE_LIGHT, MAX_SEQUENCE_STEPS),
		NUM_LIGHTS
	};

	PolySequencerModule(void) : param_helper(this) {
		rack::random::init();

		reset_to_saved_state = std::make_shared<bool>(false);

		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(CLK_INPUT, "Clock");
		configInput(RESET_INPUT, "Reset");
		configOutput(SEQ_A_CLK_OUTPUT, "Sequence A trigger");
		configOutput(SEQ_B_CLK_OUTPUT, "Sequence B trigger");
		configOutput(SEQ_C_CLK_OUTPUT, "Sequence C trigger");
		configOutput(SEQ_A_OUTPUT, "Sequence A");
		configOutput(SEQ_B_OUTPUT, "Sequence B");
		configOutput(SEQ_C_OUTPUT, "Sequence C");

		// Sequence values
		for (size_t i = 0; i < MAX_SEQUENCE_STEPS; i++) {
			std::string label = std::to_string(i + 1);
			configParam(SEQ_A_PARAM + i, -1.0f, 1.0f, 0.0f, "Step " + label);
			configParam(SEQ_B_PARAM + i, -1.0f, 1.0f, 0.0f, "Step " + label);
			configParam(SEQ_C_PARAM + i, -1.0f, 1.0f, 0.0f, "Step " + label);
		}

		// Dividers and routing grid
		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			std::string label = std::to_string(i + 1);

			configInput(DIVIDER_INPUT + i, "Divider " + label);

			configParam(DIVIDER_PARAM + i, 1.0f, 16.0f, 1.0f, "Rhythm Divider " + label);

			configSwitch(DIVIDER_TO_SEQ_A_PARAM + i, 0.0f, 1.0f, 0.0f, "Divider " + label + " to A", {"OFF", "ON"});
			configSwitch(DIVIDER_TO_SEQ_B_PARAM + i, 0.0f, 1.0f, 0.0f, "Divider " + label + " to B", {"OFF", "ON"});
			configSwitch(DIVIDER_TO_SEQ_C_PARAM + i, 0.0f, 1.0f, 0.0f, "Divider " + label + " to C", {"OFF", "ON"});
		}

		configSwitch(SEQ_A_RANGE_PARAM, 0.0f, 2.0f, 1.0f, "Sequence A Range", {"±1 V", "±2 V", "±5 V"});
		configSwitch(SEQ_B_RANGE_PARAM, 0.0f, 2.0f, 1.0f, "Sequence B Range", {"±1 V", "±2 V", "±5 V"});
		configSwitch(SEQ_C_RANGE_PARAM, 0.0f, 2.0f, 1.0f, "Sequence C Range", {"±1 V", "±2 V", "±5 V"});

		configSwitch(SUMMING_MODE_PARAM, 0.0f, 1.0f, 0.0f, "Sum Mode", {"OR", "XOR"});
		configParam(NUM_STEPS_PARAM, 1.0f, 8.0f, 4.0f, "Steps");  // Not visible
		configButton(RESET_PARAM, "Reset");
		configButton(NEXT_PARAM, "Next");

		light_divider.setDivision(512);
		reset_squelch_pulse.trigger(SQUELCH_TIME);

		onReset();
	}

	~PolySequencerModule(void) { settings.save(); }

	void onReset(void) override {
		reset_squelch_pulse.trigger(SQUELCH_TIME);
		clock_trigger.reset();
		reset_trigger.reset();
		for (auto& divider : rhythm_divider) {
			divider.reset();
		}

		seq_a_clock_pulse.reset();
		seq_b_clock_pulse.reset();
		seq_c_clock_pulse.reset();

		seq_a_state = 0;
		seq_b_state = 0;
		seq_c_state = 0;

		seq_a_divisions.fill(false);
		seq_b_divisions.fill(false);
		seq_c_divisions.fill(false);
		rhythm_divider_triggered.fill(false);

		saved_state = SavedSequencerState();
		reset_hold_state = SavedSequencerState();

		updateOutputs();
	}

	// Serialize state not captured by params
	json_t* dataToJson() override {
		json_t* root = json_object();

		// Sequencer state
		json_t* states = json_array();
		json_array_append_new(states, json_integer(static_cast<int>(seq_a_state)));
		json_array_append_new(states, json_integer(static_cast<int>(seq_b_state)));
		json_array_append_new(states, json_integer(static_cast<int>(seq_c_state)));
		json_object_set_new(root, "states", states);

		// Divider internal state
		json_t* dividers = json_array();
		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			json_array_insert_new(dividers, i, json_integer(static_cast<int>(rhythm_divider[i].clock)));
		}
		json_object_set_new(root, "dividers", dividers);

		// Reset to saved state
		json_object_set_new(root, "reset_to_saved_state", json_boolean(*reset_to_saved_state));

		// Save state
		json_t* saved_state_json = json_object();

		// -- Sequencer states
		json_t* saved_state_states = json_array();
		json_array_append_new(saved_state_states, json_integer(static_cast<int>(saved_state.seq_a_state)));
		json_array_append_new(saved_state_states, json_integer(static_cast<int>(saved_state.seq_b_state)));
		json_array_append_new(saved_state_states, json_integer(static_cast<int>(saved_state.seq_c_state)));
		json_object_set_new(saved_state_json, "states", saved_state_states);

		// -- Divider internal state
		json_t* saved_state_dividers = json_array();
		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			json_array_insert_new(saved_state_dividers, i,
								  json_integer(static_cast<int>(saved_state.rhythm_divider_states[i])));
		}
		json_object_set_new(saved_state_json, "dividers", saved_state_dividers);

		json_object_set_new(root, "saved_state", saved_state_json);

		return root;
	}

	// Deserialize state not captured by params
	void dataFromJson(json_t* root) override {
		// Sequencer state
		json_t* states = json_object_get(root, "states");
		if (states) {
			json_t* state = json_array_get(states, 0);
			if (state) {
				seq_a_state = static_cast<size_t>(json_integer_value(state));
			}

			state = json_array_get(states, 1);
			if (state) {
				seq_b_state = static_cast<size_t>(json_integer_value(state));
			}

			state = json_array_get(states, 2);
			if (state) {
				seq_c_state = static_cast<size_t>(json_integer_value(state));
			}
		}

		// Divider internal state
		json_t* dividers = json_object_get(root, "dividers");
		if (dividers) {
			for (size_t i = 0; i < NUM_DIVIDERS; i++) {
				json_t* divider = json_array_get(dividers, i);
				if (divider) {
					rhythm_divider[i].clock = static_cast<uint32_t>(json_integer_value(divider));
				}
			}
		}

		// Reset to saved state
		json_t* reset_to_saved_json = json_object_get(root, "reset_to_saved_state");
		if (reset_to_saved_json) {
			*reset_to_saved_state = json_boolean_value(reset_to_saved_json);
		}

		// Save state
		json_t* saved_state_json = json_object_get(root, "saved_state");
		if (saved_state_json) {
			// -- Sequencer states
			json_t* saved_state_states = json_object_get(saved_state_json, "states");
			if (saved_state_states) {
				json_t* state = json_array_get(saved_state_states, 0);
				if (state) {
					saved_state.seq_a_state = static_cast<size_t>(json_integer_value(state));
				}

				state = json_array_get(saved_state_states, 1);
				if (state) {
					saved_state.seq_b_state = static_cast<size_t>(json_integer_value(state));
				}

				state = json_array_get(saved_state_states, 2);
				if (state) {
					saved_state.seq_c_state = static_cast<size_t>(json_integer_value(state));
				}
			}

			// -- Divider internal state
			json_t* dividers = json_object_get(saved_state_json, "dividers");
			if (dividers) {
				for (size_t i = 0; i < NUM_DIVIDERS; i++) {
					json_t* divider = json_array_get(dividers, i);
					if (divider) {
						saved_state.rhythm_divider_states[i] = static_cast<uint32_t>(json_integer_value(divider));
					}
				}
			}
		}
	}

	// Return the value and range param IDs for a given track
	static void getParamsForTrack(const size_t track, size_t& value_param, size_t& range_param) {
		switch (track) {
			case 0:
				value_param = SEQ_A_PARAM;
				range_param = SEQ_A_RANGE_PARAM;
				break;

			case 1:
				value_param = SEQ_B_PARAM;
				range_param = SEQ_B_RANGE_PARAM;
				break;

			case 2:
				value_param = SEQ_C_PARAM;
				range_param = SEQ_C_RANGE_PARAM;
				break;

			default:
				WARN("Invalid track selected: %i", static_cast<int>(track));
				value_param = SEQ_A_PARAM;
				range_param = SEQ_A_RANGE_PARAM;
				break;
		}
	}

	// Create a JSON document in VCV Portable Sequence Format
	// for a given track.
	json_t* toPortableSequenceJson(const size_t track) {
		size_t value_param;
		size_t range_param;
		getParamsForTrack(track, value_param, range_param);

		float range = SEQ_RANGE_SCALES[rack::math::clamp(static_cast<size_t>(params[range_param].getValue()), 0,
														 slime::size(SEQ_RANGE_SCALES))];

		json_t* root = json_object();
		// FUTURE: https://jansson.readthedocs.io/en/2.13/apiref.html#building-values

		// Portable component
		json_t* vcvseq = json_object();
		json_object_set_new(vcvseq, "length", json_real(sequence_steps));

		json_t* notes = json_array();
		for (size_t i = 0; i < sequence_steps; i++) {
			json_t* note = json_object();
			json_object_set_new(note, "type", json_string("note"));
			json_object_set_new(note, "start", json_real(i));
			json_object_set_new(note, "pitch",
								// Convert to v/oct regardless of range param
								json_real(params[value_param + i].getValue() * range));
			json_object_set_new(note, "length", json_real(1.0));
			json_array_append_new(notes, note);
		}
		json_object_set_new(vcvseq, "notes", notes);

		json_object_set_new(root, "vcvrack-sequence", vcvseq);

		// Supplementary component
		json_t* subseq = json_object();
		json_object_set_new(subseq, "range", json_real(range));
		json_object_set_new(subseq, "source", json_integer(track));
		json_object_set_new(root, "SlimeChild-Substation-PolySeq-sequence", subseq);

		return root;
	}

	// Given a sequence document in JSON form, populate a track.
	void fromPortableSequenceJson(const size_t track, json_t* root) {
		size_t value_param;
		size_t range_param;
		getParamsForTrack(track, value_param, range_param);

		float range = 5.0f;

		// FUTURE: https://jansson.readthedocs.io/en/2.13/apiref.html#parsing-and-validating-values

		// Supplementary component
		json_t* subseq = json_object_get(root, "SlimeChild-Substation-PolySeq-sequence");
		if (subseq) {
			// Copied in from PolySeq; supports full feature-set.
			json_t* range_json = json_object_get(subseq, "range");
			if (range_json) {
				range = static_cast<float>(json_real_value(range_json));
			}
		}

		// Try to find an index for this range (default to first) then set the switch
		float range_index = 0.0f;
		for (size_t i = 0; i < SEQ_RANGE_SCALES.size(); i++) {
			if (abs(SEQ_RANGE_SCALES[i] - range) < 0.01f) {
				range_index = static_cast<float>(i);
			}
		}
		paramQuantities[range_param]->setValue(range_index);

		// Portable component
		json_t* vcvseq = json_object_get(root, "vcvrack-sequence");
		if (!vcvseq) {
			// No sequence data, so fail
			return;
		}

		// length
		// FUTURE: set the sequence length here (which is shared for all tracks, right now)
		/*
		json_t* length_json = json_object_get(vcvseq, "length");
		if (length_json) {
			size_t length = static_cast<size_t>(json_integer_value(length_json));

		}
		*/

		// notes
		json_t* notes_json = json_object_get(vcvseq, "notes");
		if (notes_json) {
			size_t size = json_array_size(notes_json);
			if (size > MAX_SEQUENCE_STEPS) {
				size = MAX_SEQUENCE_STEPS;
			}

			for (size_t i = 0; i < size; i++) {
				json_t* note_json = json_array_get(notes_json, i);
				if (!note_json) {
					continue;
				}

				json_t* type_json = json_object_get(note_json, "type");
				if (type_json) {
					const char* type = json_string_value(type_json);
					if (strncmp(type, "note", 4) != 0) {
						// Not a note, don't know how to parse
						continue;
					}
				}

				json_t* pitch_json = json_object_get(note_json, "pitch");
				if (pitch_json) {
					float pitch = static_cast<float>(json_real_value(pitch_json));
					paramQuantities[value_param + i]->setValue(rack::math::clamp(pitch / range, -1.0f, 1.0f));
				}
			}
		}
	}

	/** Saves the sequencer state */
	void saveSequenceState() {
		saved_state.seq_a_state = seq_a_state;
		saved_state.seq_b_state = seq_b_state;
		saved_state.seq_c_state = seq_c_state;

		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			saved_state.rhythm_divider_states[i] = rhythm_divider[i].clock;
		}
	}

	/** Restores the most recently saved sequencer state */
	void restoreSequenceState() {
		slime::engine::ModuleHistoryGuard guard(this, "restore state");

		seq_a_state = saved_state.seq_a_state;
		seq_b_state = saved_state.seq_b_state;
		seq_c_state = saved_state.seq_c_state;

		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			rhythm_divider[i].clock = saved_state.rhythm_divider_states[i];
		}
	}

	void randomizeDividers(void) {
		slime::engine::ModuleHistoryGuard guard(this, "randomize dividers");

		for (size_t i = PolySequencerModule::DIVIDER_PARAM; i <= PolySequencerModule::DIVIDER_PARAM_LAST; i++) {
			paramQuantities[i]->setScaledValue(rack::random::uniform());
		}
	}

	void randomizeGrid(void) {
		slime::engine::ModuleHistoryGuard guard(this, "randomize grid");

		uint64_t rand = rack::random::u64();
		for (size_t i = PolySequencerModule::DIVIDER_TO_SEQ_A_PARAM;
			 i <= PolySequencerModule::DIVIDER_TO_SEQ_A_PARAM_LAST; i++) {
			paramQuantities[i]->setValue(static_cast<float>(rand & 1));
			rand >>= 1;
		}

		for (size_t i = PolySequencerModule::DIVIDER_TO_SEQ_B_PARAM;
			 i <= PolySequencerModule::DIVIDER_TO_SEQ_B_PARAM_LAST; i++) {
			paramQuantities[i]->setValue(static_cast<float>(rand & 1));
			rand >>= 1;
		}

		for (size_t i = PolySequencerModule::DIVIDER_TO_SEQ_C_PARAM;
			 i <= PolySequencerModule::DIVIDER_TO_SEQ_C_PARAM_LAST; i++) {
			paramQuantities[i]->setValue(static_cast<float>(rand & 1));
			rand >>= 1;
		}
	}

	void randomizeSequence(size_t track) {
		size_t value_param;
		size_t range_param;
		PolySequencerModule::getParamsForTrack(track, value_param, range_param);

		slime::engine::ModuleHistoryGuard guard(this, "randomize sequence");

		for (size_t i = 0; i < PolySequencerModule::MAX_SEQUENCE_STEPS; i++) {
			paramQuantities[i + value_param]->setScaledValue(rack::random::uniform());
		}
	}

	void copySequenceToClipboard(size_t track) {
		json_t* seq = toPortableSequenceJson(track);
		DEFER({ json_decref(seq); });

		slime::engine::Clipboard::set(seq);
	}

	void pasteSequenceFromClipboard(size_t track) {
		json_t* doc = slime::engine::Clipboard::get<json_t*>();
		if (!doc) {
			return;
		}
		DEFER({ json_decref(doc); });

		json_t* vcvseq = json_object_get(doc, "vcvrack-sequence");
		if (vcvseq) {
			slime::engine::ModuleHistoryGuard guard(this, "paste sequence");
			fromPortableSequenceJson(track, doc);
		}
	}

	void process(const ProcessArgs& args) override {
		// Check for params requiring immediate update
		/* FUTURE: sequence length
		if (param_helper.changed(NUM_STEPS_PARAM)) {
			sequence_steps = param_helper.value(NUM_STEPS_PARAM);
		}
		*/

		// Clock-rate
		clock_trigger.process(inputs[CLK_INPUT].getVoltage());
		bool clk = clock_trigger.isRising();

		// Squelch pulse after reset
		if (reset_squelch_pulse.process(args.sampleTime)) {
			clk = false;
		}

		if (clk) {
			updateClockedParams();
			stepSequencer();

			// Update D->S lights and divider lights
			for (size_t i = 0; i < NUM_DIVIDERS; i++) {
				lights[DIVIDER_TRIGGER_LIGHT + i].setSmoothBrightness(
					rhythm_divider_triggered[i] ? 1.0f : 0.0f,
					args.sampleTime * light_divider.division * settings.light_trigger_fade);
			}
		}

		// Process reset
		reset_trigger.process(inputs[RESET_INPUT].getVoltage() + 5.0f * params[RESET_PARAM].getValue());

		// Rising edge
		if (reset_trigger.isRising()) {
			reset_squelch_pulse.trigger(SQUELCH_TIME);

			reset_hold_state = *reset_to_saved_state ? SavedSequencerState(saved_state) : SavedSequencerState();
			seq_a_state = reset_hold_state.seq_a_state;
			seq_b_state = reset_hold_state.seq_b_state;
			seq_c_state = reset_hold_state.seq_c_state;

			for (size_t i = 0; i < NUM_DIVIDERS; i++) {
				rhythm_divider[i].clock = reset_hold_state.rhythm_divider_states[i];
			}
		}

		// Process next
		if (param_helper.changed(NEXT_PARAM) && param_helper.value(NEXT_PARAM) > 0.5f) {
			// Reset-hold
			if (reset_trigger.isHigh()) {
				// State will be set in the block below
				reset_hold_state.roll(sequence_steps);
			} else {
				// Advance each track by 1
				seq_a_state = (seq_a_state + 1) % sequence_steps;
				seq_b_state = (seq_b_state + 1) % sequence_steps;
				seq_c_state = (seq_c_state + 1) % sequence_steps;
			}

			seq_a_clock_pulse.trigger();
			seq_b_clock_pulse.trigger();
			seq_c_clock_pulse.trigger();
		}

		// Reset-hold
		if (reset_trigger.isHigh()) {
			// Override the states set by stepSequence(), but still call it because it generates the TRIG pulses
			seq_a_state = reset_hold_state.seq_a_state;
			seq_b_state = reset_hold_state.seq_b_state;
			seq_c_state = reset_hold_state.seq_c_state;
		}

		// Audio-rate
		updateOutputs();
		bool seq_a_trigger = seq_a_clock_pulse.process(args.sampleTime);
		bool seq_b_trigger = seq_b_clock_pulse.process(args.sampleTime);
		bool seq_c_trigger = seq_c_clock_pulse.process(args.sampleTime);
		outputs[SEQ_A_CLK_OUTPUT].setVoltage(seq_a_trigger ? 10.0f : 0.0f);
		outputs[SEQ_B_CLK_OUTPUT].setVoltage(seq_b_trigger ? 10.0f : 0.0f);
		outputs[SEQ_C_CLK_OUTPUT].setVoltage(seq_c_trigger ? 10.0f : 0.0f);

		// Update lights
		if (light_divider.process()) {
			for (size_t i = 0; i < MAX_SEQUENCE_STEPS; i++) {
				lights[SEQ_A_STATE_LIGHT + i].setSmoothBrightness(
					(seq_a_state == i) ? 1.0f : 0.0f,
					args.sampleTime * light_divider.division * settings.light_trigger_fade);
				lights[SEQ_B_STATE_LIGHT + i].setSmoothBrightness(
					(seq_b_state == i) ? 1.0f : 0.0f,
					args.sampleTime * light_divider.division * settings.light_trigger_fade);
				lights[SEQ_C_STATE_LIGHT + i].setSmoothBrightness(
					(seq_c_state == i) ? 1.0f : 0.0f,
					args.sampleTime * light_divider.division * settings.light_trigger_fade);
			}

			for (size_t i = 0; i < NUM_DIVIDERS; i++) {
				lights[DIVIDER_TRIGGER_LIGHT + i].setSmoothBrightness(
					0.0f, args.sampleTime * light_divider.division * settings.light_trigger_fade);
			}
		}
	}

	/**
	 * Updates params that are only read on clock trigger.
	 *
	 * Updates clock divisions.
	 */
	void updateClockedParams(void) {
		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			// Update clock divisions
			uint32_t division =
				rack::math::clamp(std::round(params[DIVIDER_PARAM + i].getValue() +
											 inputs[DIVIDER_INPUT + i].getVoltage() * (0.1f * MAX_RHYTHM_DIVISIONS)),
								  1.0f, MAX_RHYTHM_DIVISIONS);
			rhythm_divider[i].setDivision(division);
			seq_a_divisions[i] = (params[DIVIDER_TO_SEQ_A_PARAM + i].getValue() > 0.5f);
			seq_b_divisions[i] = (params[DIVIDER_TO_SEQ_B_PARAM + i].getValue() > 0.5f);
			seq_c_divisions[i] = (params[DIVIDER_TO_SEQ_C_PARAM + i].getValue() > 0.5f);
			summing_xor = (params[SUMMING_MODE_PARAM].getValue() > 0.5f);
		}
	}

	/**
	 * Advances the sequencer by one step.
	 *
	 * Should by called on clock trigger.
	 */
	void stepSequencer(void) {
		bool seq_a_trigger = false, seq_b_trigger = false, seq_c_trigger = false;
		for (size_t i = 0; i < NUM_DIVIDERS; i++) {
			// Is divider triggered?
			rhythm_divider_triggered[i] = rhythm_divider[i].process();

			// Sum triggers
			if (seq_a_divisions[i]) {
				if (summing_xor) {
					seq_a_trigger = seq_a_trigger != rhythm_divider_triggered[i];
				} else {
					seq_a_trigger |= rhythm_divider_triggered[i];
				}
			}

			if (seq_b_divisions[i]) {
				if (summing_xor) {
					seq_b_trigger = seq_b_trigger != rhythm_divider_triggered[i];
				} else {
					seq_b_trigger |= rhythm_divider_triggered[i];
				}
			}

			if (seq_c_divisions[i]) {
				if (summing_xor) {
					seq_c_trigger = seq_c_trigger != rhythm_divider_triggered[i];
				} else {
					seq_c_trigger |= rhythm_divider_triggered[i];
				}
			}
		}

		// Trigger clock pulses if needed
		if (seq_a_trigger) {
			seq_a_clock_pulse.trigger();
			seq_a_state = (seq_a_state + 1) % sequence_steps;
		}

		if (seq_b_trigger) {
			seq_b_clock_pulse.trigger();
			seq_b_state = (seq_b_state + 1) % sequence_steps;
		}

		if (seq_c_trigger) {
			seq_c_clock_pulse.trigger();
			seq_c_state = (seq_c_state + 1) % sequence_steps;
		}
	}

	/**
	 * Update the value of all output ports.
	 *
	 * Should be called every sample.
	 */
	void updateOutputs(void) {
		float a_scale = SEQ_RANGE_SCALES[rack::math::clamp(static_cast<size_t>(params[SEQ_A_RANGE_PARAM].getValue()), 0,
														   slime::size(SEQ_RANGE_SCALES))];
		float b_scale = SEQ_RANGE_SCALES[rack::math::clamp(static_cast<size_t>(params[SEQ_B_RANGE_PARAM].getValue()), 0,
														   slime::size(SEQ_RANGE_SCALES))];
		float c_scale = SEQ_RANGE_SCALES[rack::math::clamp(static_cast<size_t>(params[SEQ_C_RANGE_PARAM].getValue()), 0,
														   slime::size(SEQ_RANGE_SCALES))];
		float a_out = params[SEQ_A_PARAM + (seq_a_state % MAX_SEQUENCE_STEPS)].getValue() * a_scale;
		float b_out = params[SEQ_B_PARAM + (seq_b_state % MAX_SEQUENCE_STEPS)].getValue() * b_scale;
		float c_out = params[SEQ_C_PARAM + (seq_c_state % MAX_SEQUENCE_STEPS)].getValue() * c_scale;
		outputs[SEQ_A_OUTPUT].setVoltage(a_out);
		outputs[SEQ_B_OUTPUT].setVoltage(b_out);
		outputs[SEQ_C_OUTPUT].setVoltage(c_out);
	}
};

struct PolySequencerWidget : rack::app::ModuleWidget {
	struct SequenceMenuItem : rack::ui::MenuItem {
		PolySequencerWidget* module_widget;
		size_t track;

		rack::ui::Menu* createChildMenu() override {
			rack::ui::Menu* menu = new rack::ui::Menu;

			auto* pmodule = dynamic_cast<PolySequencerModule*>(module_widget->module);
			if (!pmodule) {
				FATAL("Module was not set when appending context menu; cannot bind callbacks.");
				return menu;
			}

			auto randomize = new slime::ui::BangMenuItem;
			randomize->text = "Randomize";
			randomize->getInvoker() += std::bind(&PolySequencerModule::randomizeSequence, pmodule, track);
			menu->addChild(randomize);

			auto copy = new slime::ui::BangMenuItem;
			copy->text = "Copy portable sequence";
			copy->getInvoker() += std::bind(&PolySequencerModule::copySequenceToClipboard, pmodule, track);
			menu->addChild(copy);

			auto paste = new slime::ui::BangMenuItem;
			paste->text = "Paste portable sequence";
			paste->getInvoker() += std::bind(&PolySequencerModule::pasteSequenceFromClipboard, pmodule, track);
			menu->addChild(paste);

			return menu;
		}
	};

	struct SavedStateMenuItem : rack::ui::MenuItem {
		PolySequencerWidget* module_widget;

		rack::ui::Menu* createChildMenu() override {
			rack::ui::Menu* menu = new rack::ui::Menu;

			auto* pmodule = dynamic_cast<PolySequencerModule*>(module_widget->module);
			if (!pmodule) {
				FATAL("Module was not set when appending context menu; cannot bind callbacks.");
				return menu;
			}

			auto save = new slime::ui::BangMenuItem;
			save->text = "Save state";
			save->getInvoker() += std::bind(&PolySequencerModule::saveSequenceState, pmodule);
			menu->addChild(save);

			auto restore = new slime::ui::BangMenuItem;
			restore->text = "Restore state";
			restore->getInvoker() += std::bind(&PolySequencerModule::restoreSequenceState, pmodule);
			menu->addChild(restore);

			auto reset_restores = new slime::ui::BoolSettingMenuItem<>(pmodule->reset_to_saved_state);
			reset_restores->text = "Reset restores state";
			menu->addChild(reset_restores);

			return menu;
		}
	};

	PolySequencerWidget(PolySequencerModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(25ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/PolySeq_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/PolySeq_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::FULL)->attach(this);

		// Inputs
		const rack::math::Vec inputs[] = {{0.4f, 3.55f}, {0.4f, 4.2f}, {1.0f, 3.55f},
										  {1.6f, 3.55f}, {1.0f, 4.2f}, {1.6f, 4.2f}};
		for (size_t i = 0; i < PolySequencerModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		const rack::math::Vec outputs[] = {{2.2f, 3.55f}, {2.2f, 4.2f}, {2.8f, 4.2f},
										   {3.4f, 4.2f},  {4.0f, 4.2f}, {4.6f, 4.2f}};
		for (size_t i = 0; i < PolySequencerModule::NUM_OUTPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(outputs[i])
				->center()
				->setOutput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Knobs

		// SEQ A Values
		float x = 2.7f;
		float y = 1.2f;
		for (size_t i = PolySequencerModule::SEQ_A_PARAM; i <= PolySequencerModule::SEQ_A_PARAM_LAST; i++) {
			Knob::makeComponent(Knob::Size::SMALL)->setPositionIn(x, y)->center()->attach(module, i, this);
			x += 0.5f;
		}

		// SEQ B Values
		x = 2.7f;
		y = 1.8f;
		for (size_t i = PolySequencerModule::SEQ_B_PARAM; i <= PolySequencerModule::SEQ_B_PARAM_LAST; i++) {
			Knob::makeComponent(Knob::Size::SMALL)->setPositionIn(x, y)->center()->attach(module, i, this);
			x += 0.5f;
		}

		// SEQ C Values
		x = 2.7f;
		y = 2.4f;
		for (size_t i = PolySequencerModule::SEQ_C_PARAM; i <= PolySequencerModule::SEQ_C_PARAM_LAST; i++) {
			Knob::makeComponent(Knob::Size::SMALL)->setPositionIn(x, y)->center()->attach(module, i, this);
			x += 0.5f;
		}

		// Dividers
		x = 0.9f;
		y = 0.6f;
		for (size_t i = PolySequencerModule::DIVIDER_PARAM; i <= PolySequencerModule::DIVIDER_PARAM_LAST; i++) {
			Knob::makeComponent(Knob::Size::SMALL)
				->setPositionIn(x, y)
				->center()
				->setSnapping(true)
				->attach(module, i, this);
			y += 0.6f;
		}

		// Dividers to Seq A
		x = 1.4f;
		y = 0.6f;
		for (size_t i = PolySequencerModule::DIVIDER_TO_SEQ_A_PARAM;
			 i <= PolySequencerModule::DIVIDER_TO_SEQ_A_PARAM_LAST; i++) {
			Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
				->setPositionIn(x, y)
				->center()
				->setMomentary(false)
				->attach(module, i, this);
			y += 0.6f;
		}

		// Dividers to Seq B
		x = 1.8f;
		y = 0.6f;
		for (size_t i = PolySequencerModule::DIVIDER_TO_SEQ_B_PARAM;
			 i <= PolySequencerModule::DIVIDER_TO_SEQ_B_PARAM_LAST; i++) {
			Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
				->setPositionIn(x, y)
				->center()
				->setMomentary(false)
				->attach(module, i, this);
			y += 0.6f;
		}

		// Dividers to Seq C
		x = 2.2f;
		y = 0.6f;
		for (size_t i = PolySequencerModule::DIVIDER_TO_SEQ_C_PARAM;
			 i <= PolySequencerModule::DIVIDER_TO_SEQ_C_PARAM_LAST; i++) {
			Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
				->setPositionIn(x, y)
				->center()
				->setMomentary(false)
				->attach(module, i, this);
			y += 0.6f;
		}

		// Range
		SlideSwitch::makeComponent(SlideSwitch::Orientation::VERTICAL, SlideSwitch::Positions::TRIPLE_THROW)
			->setPositionIn(3.3f, 3.55f)
			->center()
			->attach(module, PolySequencerModule::SEQ_A_RANGE_PARAM, this);

		SlideSwitch::makeComponent(SlideSwitch::Orientation::VERTICAL, SlideSwitch::Positions::TRIPLE_THROW)
			->setPositionIn(3.9f, 3.55f)
			->center()
			->attach(module, PolySequencerModule::SEQ_B_RANGE_PARAM, this);

		SlideSwitch::makeComponent(SlideSwitch::Orientation::VERTICAL, SlideSwitch::Positions::TRIPLE_THROW)
			->setPositionIn(4.5f, 3.55f)
			->center()
			->attach(module, PolySequencerModule::SEQ_C_RANGE_PARAM, this);

		// Summing
		SlideSwitch::makeComponent(SlideSwitch::Orientation::VERTICAL, SlideSwitch::Positions::DOUBLE_THROW)
			->setPositionIn(2.7f, 3.55f)
			->center()
			->attach(module, PolySequencerModule::SUMMING_MODE_PARAM, this);

		// Reset
		Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
			->setPositionIn(2.7f, 0.6f)
			->center()
			->setMomentary(true)
			->attach(module, PolySequencerModule::RESET_PARAM, this);

		// Next
		Button::makeComponent(Button::Size::SMALL, Button::OffColor::CLEAR, Button::OnColor::GREY)
			->setPositionIn(3.7f, 0.6f)
			->center()
			->setMomentary(true)
			->attach(module, PolySequencerModule::NEXT_PARAM, this);

		// FUTURE: Number of steps

		// LEDs

		// Dividers
		x = 0.5f;
		y = 0.6f;
		for (size_t i = PolySequencerModule::DIVIDER_TRIGGER_LIGHT;
			 i <= PolySequencerModule::DIVIDER_TRIGGER_LIGHT_LAST; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionIn(x, y)
				->center()
				->attach(module, i, this);
			y += 0.6f;
		}

		// Seq A
		x = 2.7f;
		y = 0.9f;
		for (size_t i = PolySequencerModule::SEQ_A_STATE_LIGHT; i <= PolySequencerModule::SEQ_A_STATE_LIGHT_LAST; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionIn(x, y)
				->center()
				->attach(module, i, this);
			x += 0.5f;
		}

		x = 2.7f;
		y = 1.5f;
		for (size_t i = PolySequencerModule::SEQ_B_STATE_LIGHT; i <= PolySequencerModule::SEQ_B_STATE_LIGHT_LAST; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionIn(x, y)
				->center()
				->attach(module, i, this);
			x += 0.5f;
		}

		x = 2.7f;
		y = 2.1f;
		for (size_t i = PolySequencerModule::SEQ_C_STATE_LIGHT; i <= PolySequencerModule::SEQ_C_STATE_LIGHT_LAST; i++) {
			MonochromeLED::makeComponent(MonochromeLED::Size::SMALL, MonochromeLED::Color::MINT)
				->setPositionIn(x, y)
				->center()
				->attach(module, i, this);
			x += 0.5f;
		}
	}

	void appendContextMenu(rack::ui::Menu* menu) override {
		auto* pmodule = dynamic_cast<PolySequencerModule*>(this->module);
		if (!pmodule) {
			FATAL("Module was not set when appending context menu; cannot bind callbacks.");
			return;
		}

		menu->addChild(new rack::ui::MenuSeparator);

		rack::ui::MenuLabel* modelLabel = new rack::ui::MenuLabel;
		modelLabel->text = "Module Settings";
		menu->addChild(modelLabel);

		{  // Randomize Dividers
			auto item = new slime::ui::BangMenuItem;
			item->text = "Randomize Dividers";
			item->getInvoker() += std::bind(&PolySequencerModule::randomizeDividers, pmodule);
			menu->addChild(item);
		}

		{  // Randomize Grid
			auto item = new slime::ui::BangMenuItem;
			item->text = "Randomize Grid";
			item->getInvoker() += std::bind(&PolySequencerModule::randomizeGrid, pmodule);
			menu->addChild(item);
		}

		// Sequence
		for (size_t track = 0; track < 3; track++) {
			auto item = new SequenceMenuItem;
			item->text = "Sequence " + std::string({static_cast<char>('A' + track)});
			item->rightText = RIGHT_ARROW;
			item->module_widget = this;
			item->track = track;
			menu->addChild(item);
		}

		{
			auto item = new SavedStateMenuItem;
			item->text = "State";
			item->rightText = RIGHT_ARROW;
			item->module_widget = this;
			menu->addChild(item);
		}

		settings.appendContextMenu(menu);
	}
};

const std::array<float, 3> slime::plugin::substation::PolySequencerModule::SEQ_RANGE_SCALES = {1.0f, 2.0f, 5.0f};

rack::plugin::Model* modelPolySequencer =
	rack::createModel<PolySequencerModule, PolySequencerWidget>("SlimeChild-Substation-OpenSource-PolySeq");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
