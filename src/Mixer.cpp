/**********
 * Copyright (c) 2022, Coriander Violet Pines. All rights reserved.
 * This file is part of Substation by Slime Child Audio and is governed by the attached license.
 *
 * Author: C. V. Pines
 */

#include "_plugin.hpp"

#include "Settings.hpp"

#include <array>
#include <cmath>

#include <slime/Math.hpp>
#include <slime/app/ModulePanel.hpp>
#include <slime/skin/Ersatz.hpp>

#include <common.hpp>
#include <dsp/common.hpp>
#include <dsp/digital.hpp>
#include <dsp/filter.hpp>

namespace slime {
namespace plugin {
namespace substation {

struct MixerModule : slime::engine::Module {
	// Immutable config
	static const size_t NUM_CHANNELS = 3;

	// Internal state
	rack::dsp::PeakFilter level_filter;
	rack::dsp::ClockDivider level_divider, light_divider, param_divider;
	float chain_gain, drive, output_gain;
	std::array<float, NUM_CHANNELS> channel_gain;

	enum ParamIds {
		ENUMS(CHANNEL_PARAM, NUM_CHANNELS),
		ENUMS(MOD_PARAM, NUM_CHANNELS),
		LEVEL_PARAM,
		CHAIN_GAIN_PARAM,
		DRIVE_PARAM,
		NUM_PARAMS
	};

	enum InputIds {
		ENUMS(CHANNEL_INPUT, NUM_CHANNELS),
		ENUMS(CV_INPUT, NUM_CHANNELS),
		LEVEL_INPUT,
		CHAIN_INPUT,
		NUM_INPUTS
	};

	enum OutputIds { MIX_OUTPUT, CHAIN_OUTPUT, NUM_OUTPUTS };

	enum LightIds { CLIP_LIGHT, NUM_LIGHTS };

	MixerModule(void) {
		config(NUM_PARAMS, NUM_INPUTS, NUM_OUTPUTS, NUM_LIGHTS);

		configInput(LEVEL_INPUT, "Level");
		configInput(CHAIN_INPUT, "Chain");
		configOutput(MIX_OUTPUT, "Mix");
		configOutput(CHAIN_OUTPUT, "Chain");
		configBypass(CHAIN_INPUT, MIX_OUTPUT);
		configBypass(CHAIN_INPUT, CHAIN_OUTPUT);

		for (size_t i = 0; i < NUM_CHANNELS; i++) {
			std::string label = std::to_string(i + 1);

			configInput(CHANNEL_INPUT + i, "Channel " + label);
			configInput(CV_INPUT + i, "CV " + label);

			configParam(CHANNEL_PARAM + i, 0.0f, 1.0f, 0.0f, "Channel " + label + " Level", "%", 0.0f, 100.0f);
			configParam(MOD_PARAM + i, -1.0f, 1.0f, 0.0f, "Channel " + label + " Modulation", "%", 0.0f, 100.0f);
		}

		configParam(LEVEL_PARAM, 0.0f, 1.0f, 1.0f, "Mix Level", "%", 0.0f, 100.0f);
		configParam(CHAIN_GAIN_PARAM, 0.0f, 1.0f, 1.0f, "Chain Input Gain", "%", 0.0f, 100.0f);
		configParam(DRIVE_PARAM, 0.0f, 1.0f, 0.0f, "Drive", "", 0.0f, 1.0f);

		light_divider.setDivision(512);
		param_divider.setDivision(64);
		level_divider.setDivision(64);
		level_filter.setLambda(5.0f);

		onReset();
	}

	void onReset(void) override {
		level_filter.reset();
		level_divider.reset();
		param_divider.reset();
		light_divider.reset();
		chain_gain = 1.0f;
		channel_gain.fill(1.0f);
		drive = 9.5f;
		output_gain = 1.0f;
	}

	void process(const ProcessArgs& args) override {
		if (param_divider.process()) {
			chain_gain = params[CHAIN_GAIN_PARAM].getValue();
			chain_gain *= chain_gain;

			for (size_t i = 0; i < NUM_CHANNELS; i++) {
				float ch_gain = params[CHANNEL_PARAM + i].getValue();
				ch_gain *= ch_gain;

				if (inputs[CV_INPUT + i].isConnected()) {
					ch_gain += inputs[CV_INPUT + i].getVoltage() * params[MOD_PARAM + i].getValue() * 0.1f;
				}

				ch_gain = rack::math::clamp(ch_gain, 0.0f, 1.0f);
				channel_gain[i] = ch_gain;
			}

			drive = 9.5f - 9.0f * params[DRIVE_PARAM].getValue();

			output_gain = params[LEVEL_PARAM].getValue() + inputs[LEVEL_INPUT].getVoltage() * 0.1f;
			output_gain = std::min(std::max(output_gain, 0.0f), 1.0f);
			output_gain *= output_gain;
		}

		float signal = inputs[CHAIN_INPUT].getVoltageSum() * chain_gain;
		for (size_t i = 0; i < NUM_CHANNELS; i++) {
			signal += inputs[CHANNEL_INPUT + i].getVoltageSum() * channel_gain[i];
		}

		outputs[CHAIN_OUTPUT].setVoltage(signal);

		float clipped = 9.0f * slime::math::tanh_rational5(signal / drive);
		outputs[MIX_OUTPUT].setVoltage(clipped * output_gain);

		if (level_divider.process()) {
			level_filter.process(args.sampleTime * static_cast<float>(level_divider.division),
								 std::abs(clipped - signal));
		}

		if (light_divider.process()) {
			lights[CLIP_LIGHT].setBrightness(level_filter.out - 1.0f);
		}
	}  // namespace substation
};     // namespace substation

struct MixerWidget : rack::app::ModuleWidget {
	MixerWidget(MixerModule* module) {
		setModule(module);

		// Panel
		app::ModulePanel::makePanel(13ull)
			->addImageBackground(rack::asset::plugin(pluginInstance, "res/Mixer_400z.png"))
			->addSvgBackground(rack::asset::plugin(pluginInstance, "res/Mixer_Text.svg"))
			->attach(this);

		using namespace skin::ersatz;

		// Test build warning
#if defined(SLIME_DEBUG)
		TestBuildWarning::makeComponent()->attach(this);
#endif

		// Logo
		BrandLogo::makeComponent(BrandLogo::Size::FULL)->attach(this);

		// Inputs
		rack::math::Vec inputs[] = {{0.4f, 4.2f},  {1.0f, 4.2f},  {1.6f, 4.2f},  {0.4f, 3.55f},
									{1.0f, 3.55f}, {1.6f, 3.55f}, {2.2f, 3.55f}, {0.4f, 2.35f}};
		for (size_t i = 0; i < MixerModule::NUM_INPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(inputs[i])
				->center()
				->setInput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Outputs
		rack::math::Vec outputs[] = {{2.2f, 4.2f}, {1.6f, 2.35f}};
		for (size_t i = 0; i < MixerModule::NUM_OUTPUTS; i++) {
			Port::makeComponent()
				->setPositionIn(outputs[i])
				->center()
				->setOutput()
				->setCustomColors(settings.cable_colors)
				->attach(module, i, this);
		}

		// Knobs
		rack::math::Vec knobs[] = {{0.4f, 0.8f}, {1.0f, 0.8f}, {1.6f, 0.8f},  {0.4f, 1.6f}, {1.0f, 1.6f},
								   {1.6f, 1.6f}, {2.2f, 0.8f}, {1.0f, 2.35f}, {2.2f, 2.35f}};
		Knob::Size knob_sizes[] = {Knob::Size::MEDIUM, Knob::Size::MEDIUM, Knob::Size::MEDIUM,
								   Knob::Size::SMALL,  Knob::Size::SMALL,  Knob::Size::SMALL,
								   Knob::Size::MEDIUM, Knob::Size::SMALL,  Knob::Size::SMALL};
		for (size_t i = 0; i < MixerModule::NUM_PARAMS; i++) {
			Knob::makeComponent(knob_sizes[i])->setPositionIn(knobs[i])->center()->attach(module, i, this);
		}

		// LEDs
		MonochromeLED::makeComponent(MonochromeLED::Size::MEDIUM, MonochromeLED::Color::MINT)
			->setPositionIn(2.2f, 1.6f)
			->center()
			->attach(module, MixerModule::CLIP_LIGHT, this);
	}

	void appendContextMenu(rack::ui::Menu* menu) override { settings.appendContextMenu(menu); }
};

rack::plugin::Model* modelMixer = rack::createModel<MixerModule, MixerWidget>("SlimeChild-Substation-OpenSource-Mixer");

}  // namespace substation
}  // namespace plugin
}  // namespace slime
